document.addEventListener("DOMContentLoaded", function() {
  var app = new Vue({
    // Application root
    // ==============================================================
    el: "#app",
    mounted: function() {
      var video1 = document.getElementById("video1");
      var video2 = document.getElementById("video2");
      console.log(video1);
      video1.src = "vids/piano_avec_friction.mp4";
      video2.src = "vids/piano_sans_friction.mp4";
    },

    // Application Data
    // ==============================================================
    data: {
      title: "Seuil de discrimination du timbre au piano",
      section1: {
        title: "Étude pilote",
        message: "Avant de poursuivre, complétez le formulaire suivant : ",
        linkText: "Formulaire d'inscription",
        href: "https://goo.gl/forms/sneaij5k6Z7U2L0H3"
      },
      section2: {
        title: "Objectifs de l'étude",
        desc1:
          "Comprendre l’effet de la variation du poids (appliqué sur la touche) sur le mécanisme de double échappement du piano à queue.",
        desc2:
          "Évaluer si la variation de poids a un impact sur le timbre du son au piano, indépendamment du niveau d’intensité du son.",
        desc3:
          "Identifier les dimensions du timbre au piano qui varient avec le poids.",
        desc4: "Investiguer l’effet du poids sur l’articulation."
      },
      section3: {
        title: "Le mécanisme du piano",
        desc:
          "Ce schéma vous permet de situer certaines pièces utiles pour comprendre l'effet du poids. Le rouleau (8) et le bâton d'échappement (5) sont deux pièces dont le mouvement diffère selon le poids appliqué."
      },
      section4: {
        title: "Le mécanisme en mouvement",
        titleSF:
          "Sans poids: Quand la touche est enfoncée sans poids, le bâton d’échappement reste aligné avec le rouleau.",
        titleAF:
          "Avec poids: Quand la touche est enfoncée avec poids, le bâton d’échappement glisse le long du rouleau (couvert de cuir) aligné avec le rouleau.",
        desc:
          "Ces images vidéos captées à haute vitesse servent à montrer le mécanisme d'enclanchement du marteau lorsqu'il se retrouve dans une situation avec et sans poids. Remarquez la différence. Par la suite, vous pourrez demander au chercheur de vous montrer les modèles disponibles dans le laboratoire."
      },
      section5: {
        title: "Perception tactile du pianiste",
        desc1:
          "Quand le pianiste applique plus de poids, la touche est enfoncée jusqu’au fond du clavier et le pianiste ressent de la résistance ainsi qu’une sensation de friction au moment de passer le point d’échappement (« bump »), après approximativement 7 millimètres de descente.",
        subtitle1: "Le son est perçu comme étant plus rond et plus plein.",
        desc2:
          "Quand le pianiste applique moins de poids, la touche est jouée davantage à la surface du clavier, et la sensation de résistance/friction n’est pas ressentie.",
        subtitle2: "Le son est perçu comme étant plus brillant et plus léger."
      },
      section6: {
        title: "Test auditif: Exemples",
        desc1:
          "Les différences de timbre induites par la quantité de poids appliquée sur la touche sont subtiles et le test auditif que vous allez effectuer vise à vérifier si ces différences de timbre sont perceptibles par les pianistes.",
        desc2:
          "Lors du test, vous allez entendre des paires de sons qui peuvent varier en poids ou non. Écoutez ces exemples.",
        subtitle1: "Paire 1: Accroissement de poids (Sans poids -> Avec poids)",
        subtitle2: "Paire 2: Pas de changement (Sans poids -> Sans poids)",
        subtitle3: "Paire 3: Pas de changement (Avec poids -> Avec poids)"
      },
      section7: {
        title: "Test auditif: Entraînement",
        desc1:
          "Cette série d’entraînement précède le test auditif. Veuillez indiquez si vous percevez un changement de poids entre les deux sons de chaque paire. Vous pouvez écouter les enregistrements plusieurs fois et réajuster vos réponses. Quand la série est complétée, veuillez demander le corrigé des « bonnes réponses » au chercheur.",
        desc2:
          "OUI = changement de poids (sans poids -> avec poids)         NON = pas de changement de poids",
        audios: [
          {
            name: "train1",
            file: "audios/train1.wav",
            type: "audio/wav",
            answer: true
          },
          {
            name: "train2",
            file: "audios/train2.wav",
            type: "audio/wav",
            answer: false
          },
          {
            name: "train3",
            file: "audios/train3.wav",
            type: "audio/wav",
            answer: false
          },
          {
            name: "train4",
            file: "audios/train4.wav",
            type: "audio/wav",
            answer: true
          },
          {
            name: "train5",
            file: "audios/train5.wav",
            type: "audio/wav",
            answer: true
          },
          {
            name: "train6",
            file: "audios/train6.wav",
            type: "audio/wav",
            answer: false
          } //Si true => accroissement de poids vrai
        ]
      },
      section8: {
        title: "Test auditif - série 1/3",
        desc1:
          "Le test auditif comprend 3 séries correspondant à 3 niveaux de nuance : doux, moyen et fort. Veuillez indiquez si vous percevez un changement de poids entre les deux sons de chaque paire. Vous pouvez écouter les enregistrements plusieurs fois et réajuster vos réponses.",
        desc2:
          "OUI = changement de poids (sans poids -> avec poids)  NON = pas de changement de poids",
        desc3: "Première série - nuance : doux",
        audios: [
          {
            name: "essai1",
            file: "audios/essai1.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai2",
            file: "audios/essai2.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai3",
            file: "audios/essai3.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai4",
            file: "audios/essai4.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai5",
            file: "audios/essai5.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai6",
            file: "audios/essai6.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai7",
            file: "audios/essai7.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai8",
            file: "audios/essai8.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai9",
            file: "audios/essai9.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai10",
            file: "audios/essai10.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai11",
            file: "audios/essai11.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai12",
            file: "audios/essai12.wav",
            type: "audio/wav",
            value: ""
          }
        ]
      },
      section9: {
        title: "Test auditif - série 2/3",
        desc1:
          "Le test auditif comprend 3 séries correspondant à 3 niveaux de nuance : doux, moyen et fort. Veuillez indiquez si vous percevez un changement de poids entre les deux sons de chaque paire. Vous pouvez écouter les enregistrements plusieurs fois et réajuster vos réponses.",
        desc2:
          "OUI = changement de poids (sans poids -> avec poids)  NON = pas de changement de poids",
        desc3: "Deuxième série - nuance : moyen",
        audios: [
          {
            name: "essai13",
            file: "audios/essai13.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai14",
            file: "audios/essai14.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai15",
            file: "audios/essai15.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai16",
            file: "audios/essai16.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai17",
            file: "audios/essai17.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai18",
            file: "audios/essai18.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai19",
            file: "audios/essai19.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai20",
            file: "audios/essai20.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai21",
            file: "audios/essai21.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai22",
            file: "audios/essai22.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai23",
            file: "audios/essai23.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai24",
            file: "audios/essai24.wav",
            type: "audio/wav",
            value: ""
          }
        ]
      },
      section10: {
        title: "Test auditif - série 3/3",
        desc1:
          "Le test auditif comprend 3 séries correspondant à 3 niveaux de nuance : doux, moyen et fort. Veuillez indiquez si vous percevez un changement de poids entre les deux sons de chaque paire. Vous pouvez écouter les enregistrements plusieurs fois et réajuster vos réponses.",
        desc2:
          "OUI = changement de poids (sans poids -> avec poids)  NON = pas de changement de poids",
        desc3: "Troisième série - nuance : fort",
        audios: [
          {
            name: "essai25",
            file: "audios/essai25.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai26",
            file: "audios/essai26.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai27",
            file: "audios/essai27.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai28",
            file: "audios/essai28.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai29",
            file: "audios/essai29.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai30",
            file: "audios/essai30.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai31",
            file: "audios/essai31.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai32",
            file: "audios/essai32.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai33",
            file: "audios/essai33.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai34",
            file: "audios/essai34.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai35",
            file: "audios/essai35.wav",
            type: "audio/wav",
            value: ""
          },
          {
            name: "essai36",
            file: "audios/essai36.wav",
            type: "audio/wav",
            value: ""
          }
        ]
      },
      section11: {
        title: "Informations complémentaires",
        desc:
          "Afin de lier vos données du test avec celles du formulaire, veuillez entrer votre adresse courriel.",
        email: ""
      },
      section12: {
        title: "Questionnaire post-expérimental",
        desc:
          "Veuillez vous rediriger vers la page 'Google formulaire' afin de compléter quelques questions. \nEnvoyez vos résultats avant de quitter la fenêtre."
      }
    },

    // Methods
    // ==============================================================
    methods: {
      /**
       *
       */
      onSubmitResultsHandler: function() {
        var data = this.$data;
        audios1 = data.section8.audios;
        audios2 = data.section9.audios;
        audios3 = data.section10.audios;

        var output = "";

        var procedure = function(audio) {
          if (audio.value === "") {
            value = "--";
          } else if (audio.value === "TRUE") {
            value = "OUI";
          } else {
            value = "NON";
          }
          output += audio.name + " : " + value + "\n";
        };

        output += "RÉSULTATS SÉRIE 1\n";
        audios1.forEach(procedure);
        output += "RÉSULTATS SÉRIE 2\n";
        audios2.forEach(procedure);
        output += "RÉSULTATS SÉRIE 3\n";
        audios3.forEach(procedure);
        output += "EMAIL :: " + this.section11.email;

        console.log(output);
      },

      /**
       *
       */
      playAudio: function(id) {
        console.log(id);
        $("#" + id).trigger("play");
      }
    },

    // Filters
    // ==============================================================
    filters: {
      capitalize: function(value) {
        if (!value) return "";
        value = value.toString();
        return value.toUpperCase();
      }
    }
  });
});
